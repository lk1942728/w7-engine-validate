<?php

namespace W7\Validate;

use Itwmw\Validation\Factory;
use Itwmw\Validation\Support\Collection\Collection;
use Itwmw\Validation\Support\Interfaces\ImplicitRule;
use Itwmw\Validation\Support\Rule\OrRule;
use Itwmw\Validation\Support\Str;
use Itwmw\Validation\Support\ValidationException;
use Itwmw\Validation\ValidationData;
use W7\Validate\Exception\ValidateException;
use W7\Validate\Exception\ValidateRuntimeException;
use W7\Validate\Support\Common;
use W7\Validate\Support\Concerns\MessageProviderInterface;
use W7\Validate\Support\Concerns\ProcessorInterface;
use W7\Validate\Support\DataAttribute;
use W7\Validate\Support\Event\ValidateEventAbstract;
use W7\Validate\Support\MessageProvider;
use W7\Validate\Support\Processor\DataProcessor;
use W7\Validate\Support\Processor\ProcessorExecCond;
use W7\Validate\Support\Processor\ProcessorOptions;
use W7\Validate\Support\Processor\ProcessorParams;
use W7\Validate\Support\Processor\ProcessorSupport;
use W7\Validate\Support\Rule\BaseRule;
use W7\Validate\Support\Storage\ValidateConfig;
use W7\Validate\Support\ValidateScene;

class Validate
{
    /**
     * 全部原始验证规则.
     *
     * @see https://v.neww7.com/5/Validate.html#rule
     *
     * @var array
     */
    protected $rule = [];

    /**
     * 验证场景定义.
     *
     * @see https://v.neww7.com/5/Validate.html#scene
     *
     * @var array<string, array<string>|ValidateScene>
     */
    protected $scene = [];

    /**
     * 验证字段的名称.
     *
     * @see https://v.neww7.com/5/Validate.html#customattributes
     *
     * @var array
     */
    protected $customAttributes = [];

    /**
     * 验证失败的错误消息.
     *
     * @see https://v.neww7.com/5/Validate.html#message
     *
     * @var array
     */
    protected $message = [];

    /**
     * 当前类中自定义方法验证失败后的错误消息.
     *
     * @see https://v.neww7.com/5/Validate.html#rulemessage
     *
     * @var array
     */
    protected $ruleMessage = [];

    /**
     * 预定义正则表达式验证规则.
     *
     * @see https://v.neww7.com/5/Validate.html#regex
     *
     * @var array
     */
    protected $regex = [];

    /**
     * 验证规则组.
     *
     * @see https://v.neww7.com/5/Validate.html#group
     *
     * @var array
     */
    protected $group = [];

    /**
     * 验证器下的全局事件.
     *
     * @see https://v.neww7.com/5/Validate.html#event
     *
     * @var array
     */
    protected $event = [];

    /**
     * 所有验证的字段在存在时不能为空.
     *
     * @see https://v.neww7.com/5/Validate.html#filled
     *
     * @var bool
     */
    protected $filled = true;

    /**
     * 为字段定义一个后置数据处理器，验证完毕后触发.
     *
     * @see https://v.neww7.com/5/Validate.html#postprocessor
     *
     * @var array{string, scalar|callable|\Closure|array<scalar|callable|ProcessorSupport|ProcessorParams|\W7\Validate\Support\Processor\ProcessorOptions>}
     */
    protected $postprocessor = [];

    /**
     * 为字段定义一个后置数据处理器，验证之前触发.
     *
     * @see https://v.neww7.com/5/Validate.html#preprocess
     *
     * @var array{string, scalar|\Closure|callable|array<scalar|callable|ProcessorSupport|ProcessorParams|\W7\Validate\Support\Processor\ProcessorOptions>}
     */
    protected $preprocessor = [];

    /**
     * 为验证失败设置一个自定义异常类.
     *
     * @see https://v.neww7.com/5/Validate.html#exceptions
     *
     * @var array<class-string<\Throwable>,string|array<string>>|class-string<\Throwable>
     */
    protected $exceptions;

    /**
     * 事件优先级.
     */
    private bool $eventPriority = true;

    /**
     * 本次验证需要处理的事件.
     */
    private array $events = [];

    /**
     * 本次验证需要处理的前置场景验证事件.
     */
    private array $befores = [];

    /**
     * 本次验证需要处理的后置场景验证事件.
     */
    private array $afters = [];

    /**
     * 本次验证之前需要处理的数据处理器.
     *
     * @var array{string, mixed}
     */
    private array $preprocessors = [];

    /**
     * 本次验证之后需要处理的数据处理器.
     */
    private array $postprocessors = [];

    /**
     * 错误消息提供者.
     */
    private ?MessageProviderInterface $messageProvider = null;

    /**
     * 当前验证的原始数据.
     */
    private array $checkData = [];

    /**
     * 本次验证通过后的数据.
     */
    private array $validatedData = [];

    /**
     * 本次验证的字段名称.
     *
     * @var array<string>
     */
    private array $validateFields = [];

    /**
     * 当前验证场景名称.
     */
    private ?string $currentScene = null;

    /**
     * 使用正则表达式来验证的规则.
     *
     * @var array<string>
     */
    private array $regexRule = ['regex', 'not_regex'];

    /**
     * 扩展的普通规则.
     *
     * @see https://v.neww7.com/5/Rule.html#extend-%E6%89%A9%E5%B1%95%E6%96%B9%E6%B3%95
     */
    private array $extensions = [];

    /**
     * 扩展的隐式规则.
     *
     * @see https://v.neww7.com/5/Rule.html#extendimplicit-%E9%9A%90%E5%BC%8F%E6%89%A9%E5%B1%95
     */
    private array $implicitExtensions = [];

    /**
     * 扩展的依赖性规则.
     *
     * @see https://v.neww7.com/5/Rule.html#extenddependent-%E4%BE%9D%E8%B5%96%E6%80%A7%E9%AA%8C%E8%AF%81%E5%99%A8
     */
    private array $dependentExtensions = [];

    /**
     * 扩展的错误信息替换器.
     *
     * @see https://v.neww7.com/5/Rule.html#replacer-%E9%94%99%E8%AF%AF%E4%BF%A1%E6%81%AF%E6%9B%BF%E6%8D%A2%E5%99%A8
     */
    private array $replacers = [];

    /**
     * 是否初始化了扩展规则.
     */
    private bool $initExtendRule = false;

    /**
     * Create a validator.
     *
     * @param array $rules            Validation rules
     * @param array $messages         Error message
     * @param array $customAttributes Field Name
     */
    public static function make(array $rules = [], array $messages = [], array $customAttributes = []): Validate
    {
        return (new static())->setRules($rules)->setMessages($messages)->setCustomAttributes($customAttributes);
    }

    /**
     * Get Validator Factory.
     */
    private static function getValidationFactory(): Factory
    {
        return ValidateConfig::instance()->getFactory();
    }

    /**
     * 自动验证
     *
     * @param array $data 待验证的数据
     *
     * @throws ValidateException
     */
    public function check(array $data): array
    {
        $this->init();
        $this->checkData = $data;
        $this->addEvent('event', $this->event);
        $this->handleEvent($data, 'beforeValidate');
        $events       = $this->events;
        $this->events = [];
        $rule         = $this->getSceneRules();
        $data         = $this->pass($data, $rule);
        $this->events = $events;
        $this->handleEvent($data, 'afterValidate');
        return $data;
    }

    private function getSelfRules(): void
    {
        if ($this->initExtendRule) {
            return;
        }

        $methods = get_class_methods($this);
        foreach ($methods as $method) {
            if (str_starts_with($method, 'rule')) {
                $ruleName                    = substr($method, 4);
                $ruleName                    = Str::snake($ruleName);
                $this->extensions[$ruleName] = $this->$method(...);
            }
        }
        $this->initRuleMessage();
        $this->initExtendRule = true;
    }

    /**
     * Perform data validation and processing.
     *
     * @param array $data  Data to be verified
     * @param array $rules Rules for validation
     *
     * @throws ValidateException
     */
    private function pass(array $data, array $rules): array
    {
        $this->preprocessors  = array_merge($this->preprocessor, $this->preprocessors);
        $this->postprocessors = array_merge($this->postprocessor, $this->postprocessors);

        // Validated fields are not re-validated
        $checkFields          = array_diff(array_keys($rules), $this->validateFields);
        $checkRules           = array_intersect_key($rules, array_flip($checkFields));
        $checkRules           = $this->getCheckRules($checkRules);
        $this->validateFields = array_merge($this->validateFields, $checkFields);

        if ($this->filled) {
            $checkRules = $this->addFilledRule($checkRules);
        }

        // Preprocessor and filters only handle the fields that are being validated now
        $fields = array_keys($checkRules);
        $data   = $this->processData($data, $fields, $this->preprocessors);

        if ($this->eventPriority) {
            $this->handleEvent($data, 'beforeValidate');
            $this->handleEventCallback($data, 'before');
        } else {
            $this->handleEventCallback($data, 'before');
            $this->handleEvent($data, 'beforeValidate');
        }

        $validatedData = $this->validatedData;
        if (!empty($checkRules)) {
            try {
                $validator = $this->getValidationFactory()->make($data, $checkRules, $this->message, $this->customAttributes);
                $validator->addExtensions($this->extensions);
                $validator->addImplicitExtensions($this->implicitExtensions);
                $validator->addDependentExtensions($this->dependentExtensions);
                $validator->addReplacers($this->replacers);
                $validator->setFallbackMessages($this->ruleMessage);
                $validatedData = $validator->validate();
                $validatedData = array_merge($this->validatedData, $validatedData);
            } catch (ValidationException $e) {
                throw $this->getException($e);
            }
        }

        $validatedData = $this->processData($validatedData, $fields, $this->postprocessors);

        if ($this->eventPriority) {
            $this->handleEventCallback($validatedData, 'after');
            $this->handleEvent($validatedData, 'afterValidate');
        } else {
            $this->handleEvent($validatedData, 'afterValidate');
            $this->handleEventCallback($validatedData, 'after');
        }

        $this->initScene();
        $this->scene(null);
        return $validatedData;
    }

    /**
     * Get rules from rule groups.
     */
    private function getRuleFromRuleGroup(string $name): array|false
    {
        if (isset($this->group[$name])) {
            $ruleGroup = $this->group[$name];
            if (!is_array($ruleGroup)) {
                $ruleGroup = explode('|', $ruleGroup);
            }
            return $ruleGroup;
        }

        return false;
    }

    /**
     * 将原始规则转为验证器规则.
     *
     * @param array|null $rules 原始规则，如果为null则获取全部的初始规则
     *
     * @return array|array[]
     */
    public function getCheckRules(array $rules = null): array
    {
        if (is_null($rules)) {
            $rules = $this->getInitialRules();
        }
        $rulesFields = array_keys($rules);
        $rule        = array_map(function ($rules, $field) {
            if (!is_array($rules)) {
                $rules = explode('|', $rules);
            }

            $rules = array_reduce($rules, function ($rules, $rule) {
                if (is_string($rule) && ($ruleGroup = $this->getRuleFromRuleGroup($rule))) {
                    $rules = array_merge($rules, $ruleGroup);
                } else {
                    $rules[] = $rule;
                }
                return $rules;
            }, []);

            return array_map(function ($rule) use ($field) {
                if (is_string($rule)) {
                    if ('or' === $rule) {
                        throw new \InvalidArgumentException('Validation rule or requires at least 1 parameters.');
                    }
                    if (str_starts_with($rule, 'or:')) {
                        $ruleInfo = Common::getKeyAndParam($rule, true);
                        if (empty($ruleInfo[1])) {
                            throw new \InvalidArgumentException('Validation rule or requires at least 1 parameters.');
                        }
                        $orRules = [];
                        foreach ($ruleInfo[1] as $item) {
                            if (is_string($item) && ($ruleGroup = $this->getRuleFromRuleGroup($item))) {
                                $orRules = array_merge($orRules, $ruleGroup);
                            } else {
                                $orRules[] = $item;
                            }
                        }
                        $orRules = array_unique($orRules);
                        return new OrRule($orRules);
                    }
                    foreach ($this->regexRule as $regexRuleName) {
                        $regexRuleName = $regexRuleName . ':';
                        if (str_starts_with($rule, $regexRuleName)) {
                            $regexName = substr($rule, strlen($regexRuleName));
                            if (isset($this->regex[$regexName])) {
                                return $regexRuleName . $this->regex[$regexName];
                            }
                        }
                    }

                    $ruleClass = $this->getRuleClass($rule);
                    if (false !== $ruleClass) {
                        if (!empty($message = $this->getMessageProvider()->getMessage($field, $rule))) {
                            $ruleClass->setMessage($message);
                        }
                        return $ruleClass;
                    }

                    return $rule;
                }

                return $rule;
            }, $rules);
        }, $rules, $rulesFields);

        return array_combine($rulesFields, $rule);
    }

    /**
     * 获取原始规则.
     *
     * @param string|null $sceneName 场景名称，如果不提供则获取当前场景名称
     */
    public function getInitialRules(?string $sceneName = ''): array
    {
        if ('' === $sceneName) {
            $sceneName = $this->currentScene;
        }

        if (empty($sceneName)) {
            return $this->rule;
        }

        if (method_exists($this, 'scene' . ucfirst($sceneName))) {
            $scene = new ValidateScene($this->rule);
            call_user_func([$this, 'scene' . ucfirst($sceneName)], $scene);
            return $scene->getRules();
        }

        if (isset($this->scene[$sceneName])) {
            if (is_array($this->scene[$sceneName])) {
                return array_intersect_key($this->rule, array_flip($this->scene[$sceneName]));
            } elseif ($this->scene[$sceneName] instanceof ValidateScene) {
                return $this->scene[$sceneName]->getRules();
            } else {
                throw new \InvalidArgumentException('The scene configuration is incorrect');
            }
        }

        return $this->rule;
    }

    /**
     * Get the instance class of a custom rule.
     *
     * @param string $ruleName Custom Rule Name
     *
     * @return false|BaseRule
     */
    private function getRuleClass(string $ruleName)
    {
        static $rulesClass = [];

        [$ruleName, $param] = Common::getKeyAndParam($ruleName, true);

        if (isset($rulesClass[$ruleName]) && 0 === $rulesClass[$ruleName]) {
            return false;
        }

        foreach (ValidateConfig::instance()->getRulePath() as $rulesPath) {
            $ruleNameSpace = $rulesPath . ucfirst($ruleName);
            if (isset($rulesClass[$ruleNameSpace])) {
                return !is_null($param) ? new $ruleNameSpace(...$param) : new $ruleNameSpace();
            } elseif (class_exists($ruleNameSpace) && is_subclass_of($ruleNameSpace, BaseRule::class)) {
                $rulesClass[$ruleNameSpace] = 1;
                return !is_null($param) ? new $ruleNameSpace(...$param) : new $ruleNameSpace();
            }
        }

        $rulesClass[$ruleName] = 0;
        return false;
    }

    /**
     * 指定当前验证器的场景.
     *
     * @return $this
     */
    final public function scene(?string $name): static
    {
        $this->currentScene = $name;
        return $this;
    }

    /**
     * 获取当前验证器的场景名称.
     */
    public function getCurrentSceneName(): ?string
    {
        return $this->currentScene;
    }

    /**
     * Add the `filled` rule.
     *
     * @param array $rules Original Rules
     */
    protected function addFilledRule(array $rules): array
    {
        $conflictRules = [
            'filled', 'nullable', 'accepted', 'accepted_if', 'declined', 'declined_if', 'present', 'required', 'required_if',
            'required_unless', 'required_with', 'required_with_all', 'required_without', 'required_without_all',
        ];

        $implicitRules = array_keys($this->implicitExtensions);
        foreach ($rules as &$rule) {
            $rulesName = array_map(function ($value) use ($implicitRules) {
                if (is_object($value)) {
                    // By default, when an attribute being validated is not present or contains an empty string,
                    // normal validation rules, including custom extensions, are not run.
                    // If the ImplicitRule interface is implemented,
                    // it means that the rule object needs to be run even if the property is empty.
                    // So there is no need for the `filled` rule either,
                    // so let there be a `filled` in the array object to skip this process.
                    if ($value instanceof ImplicitRule) {
                        return 'filled';
                    } else {
                        return '';
                    }
                }

                if (is_string($value)) {
                    $ruleName = Common::getKeyAndParam($value)[0];
                    $ruleName = Str::snake($ruleName);

                    if (in_array($ruleName, $implicitRules)) {
                        return 'filled';
                    }
                }

                return Str::snake($value);
            }, $rule);

            if (empty(array_intersect($conflictRules, $rulesName))) {
                array_unshift($rule, 'filled');
            }
        }

        return $rules;
    }

    /**
     * 设置验证场景.
     *
     * @param array<string,array<string>|ValidateScene>|null $scene 如果为null，则清空所有验证场景
     */
    public function setScene(array $scene = null): static
    {
        if (is_null($scene)) {
            $this->scene = [];
        } else {
            $this->scene = array_merge($this->scene, $scene);
        }

        return $this;
    }

    /**
     * 设置验证规则.
     *
     * @param array<string,string,array<string>>|null $rules 验证规则，如果为null，则清空所有验证规则
     *
     * @return $this
     */
    public function setRules(array $rules = null): static
    {
        if (is_null($rules)) {
            $this->rule = [];
        } else {
            $this->rule = array_merge($this->rule, $rules);
        }

        return $this;
    }

    /**
     * 设置验证消息.
     *
     * @param array<string,string|array>|null $message 验证消息，如果为null，则清空所有验证消息
     *
     * @return $this
     */
    public function setMessages(array $message = null): static
    {
        if (is_null($message)) {
            $this->message = [];
        } else {
            $this->message = array_merge($this->message, $message);
        }

        return $this;
    }

    /**
     * 设置验证字段名称.
     *
     * @param array<string,string>|null $customAttributes 验证字段名称，如果为null，则清空所有验证字段名称
     *
     * @return $this
     */
    public function setCustomAttributes(array $customAttributes = null): static
    {
        if (is_null($customAttributes)) {
            $this->customAttributes = [];
        } else {
            $this->customAttributes = array_merge($this->customAttributes, $customAttributes);
        }

        return $this;
    }

    public function getMessages(): array
    {
        return $this->message;
    }

    public function getCustomAttributes(): array
    {
        return $this->customAttributes;
    }

    protected function getException(ValidationException $e)
    {
        $messageProvider = $this->getMessageProvider();
        $attribute       = $e->getAttribute();
        $rule            = $e->getRule();
        $attributeRule   = strtolower($attribute . '.' . $rule);
        $messageProvider->setAttribute($attribute)->setRule($rule);
        $error = $messageProvider->handleMessage($e->getMessage());

        $exceptionClass = null;
        if (is_string($this->exceptions) && class_exists($this->exceptions) && is_subclass_of($this->exceptions, \Throwable::class)) {
            $exceptionClass = $this->exceptions;
        } elseif (is_array($this->exceptions) && !empty($this->exceptions)) {
            foreach ($this->exceptions as $exception => $keys) {
                if (!is_array($keys)) {
                    $keys = [$keys];
                }
                foreach ($keys as $key) {
                    if ($key === $attribute || strtolower($key) === $attributeRule) {
                        $exceptionClass = $exception;
                        break 2;
                    }
                }
            }
        }

        if (is_null($exceptionClass)) {
            return new ValidateException($error, 403, $e->getAttribute(), $e);
        } else {
            return new $exceptionClass(message: $error, code: 403, previous: $e);
        }

    }

    /**
     * 使用闭包方法来自定义验证场景并进行验证
     *
     * @throws ValidateException
     */
    public function invokeSceneCheck(callable $fn, array $data): array
    {
        $this->init();
        $this->checkData = $data;
        $this->addEvent('event', $this->event);
        $this->handleEvent($data, 'beforeValidate');
        $events       = $this->events;
        $this->events = [];

        $rules = $this->invokeSceneFunction($fn, $this->rule, $this->checkData);

        $data         = $this->pass($data, $rules);
        $this->events = $events;
        $this->handleEvent($data, 'afterValidate');
        return $data;
    }

    /**
     * Calling custom scene closures.
     */
    private function invokeSceneFunction(callable $fn, array $rule, array $data, string $sceneName = ''): array
    {
        $scene = new ValidateScene($rule, $data);
        call_user_func($fn, $scene);
        return $this->mergeSceneData($scene, $sceneName);
    }

    /**
     * 使用当前验证器下的数据来创建一个场景类.
     */
    public function makeValidateScene(): ValidateScene
    {
        return new ValidateScene($this->rule, $this->checkData);
    }

    /**
     * 合并验证场景数据到验证器中，仅在下次的自动验证生效.
     */
    private function mergeSceneData(ValidateScene $scene, string $sceneName = ''): array
    {
        $this->events         = $scene->events;
        $this->afters         = $scene->afters;
        $this->befores        = $scene->befores;
        $this->preprocessors  = $scene->preprocessors;
        $this->postprocessors = $scene->postprocessors;
        $this->eventPriority  = $scene->eventPriority;
        $next                 = $scene->next;
        $sceneRule            = $scene->getRules();
        if (!empty($next)) {
            return $this->next($scene->next, $sceneRule, $sceneName);
        }
        return $sceneRule;
    }

    /**
     * Get the rules that need to be validation in the scene.
     *
     * @param string|null $sceneName the scene name, or the current scene name if not provided
     *
     * @throws ValidateException
     */
    private function getSceneRules(?string $sceneName = ''): array
    {
        if ('' === $sceneName) {
            $sceneName = $this->getCurrentSceneName();
        }

        if (empty($sceneName)) {
            return $this->rule;
        }

        if (method_exists($this, 'scene' . ucfirst($sceneName))) {
            return $this->invokeSceneFunction([$this, 'scene' . ucfirst($sceneName)], $this->rule, $this->checkData, $sceneName);
        }

        if (isset($this->scene[$sceneName])) {
            $sceneRule = $this->scene[$sceneName];

            if ($sceneRule instanceof ValidateScene) {
                return $this->mergeSceneData($sceneRule, $sceneName);
            }

            foreach (['event', 'before', 'after'] as $eventType) {
                if (isset($sceneRule[$eventType])) {
                    $callback = $sceneRule[$eventType];
                    $this->addEvent($eventType, $callback);
                    unset($sceneRule[$eventType]);
                }
            }

            if (!empty($sceneRule['next'])) {
                $next = $sceneRule['next'];
                unset($sceneRule['next']);
                $rules = Common::getRulesAndFill($this->rule, $sceneRule);
                return $this->next($next, $rules, $sceneName);
            } else {
                return Common::getRulesAndFill($this->rule, $sceneRule);
            }
        }

        return $this->rule;
    }

    /**
     * Processing the next scene.
     *
     * @param string $next             Next scene name or scene selector
     * @param array  $rules            Validation rules
     * @param string $currentSceneName Current scene name
     *
     * @throws ValidateException
     */
    private function next(string $next, array $rules, string $currentSceneName = ''): array
    {
        if ($next === $currentSceneName) {
            throw new ValidateRuntimeException('The scene used cannot be the same as the current scene.');
        }

        // Pre-validation
        $data                = $this->pass($this->checkData, $rules);
        $this->validatedData = array_merge($this->validatedData, $data);

        // If a scene selector exists
        if (method_exists($this, lcfirst($next) . 'Selector')) {
            $next = call_user_func([$this, lcfirst($next) . 'Selector'], $this->validatedData);
            if (is_array($next)) {
                return Common::getRulesAndFill($this->rule, $next);
            }
        }

        if (empty($next)) {
            return [];
        }

        $this->scene($next);
        return $this->getSceneRules($next);
    }

    /**
     * Processing method.
     *
     * @param string $type 'before' or 'after'
     *
     * @throws ValidateException
     */
    private function handleEventCallback(array $data, string $type)
    {
        switch ($type) {
            case 'before':
                $callbacks = $this->befores;
                break;
            case 'after':
                $callbacks = $this->afters;
                break;
        }

        if (empty($callbacks)) {
            return;
        }

        foreach ($callbacks as $callback) {
            [$callback, $param] = $callback;
            if (!is_callable($callback)) {
                $callback = $type . ucfirst($callback);
                if (!method_exists($this, $callback)) {
                    throw new ValidateRuntimeException('Method Not Found');
                }
                $callback = [$this, $callback];
            }

            if (($result = call_user_func($callback, $data, ...$param)) !== true) {
                if (isset($this->message[$result])) {
                    $result = $this->getMessageProvider()->handleMessage($this->message[$result]);
                }
                throw new ValidateException($result, 403);
            }
        }
    }

    /**
     * validate event handling.
     *
     * @param array  $data   Validated data
     * @param string $method Event Name
     *
     * @throws ValidateException
     */
    private function handleEvent(array $data, string $method): void
    {
        if (empty($this->events)) {
            return;
        }

        foreach ($this->events as $events) {
            [$callback, $param] = $events;
            if (class_exists($callback) && is_subclass_of($callback, ValidateEventAbstract::class)) {
                /** @var ValidateEventAbstract $handler */
                $handler            = new $callback(...$param);
                $handler->sceneName = $this->getCurrentSceneName();
                $handler->data      = $data;
                if (true !== call_user_func([$handler, $method])) {
                    $message = $handler->message;
                    if (isset($this->message[$message])) {
                        $message = $this->getMessageProvider()->handleMessage($this->message[$message]);
                    }
                    throw new ValidateException($message, 403);
                }
            } else {
                throw new ValidateRuntimeException('Event error or nonexistence');
            }
        }
    }

    private function processData(array $data, array $fields, array $processors = []): array
    {
        if (empty($processors)) {
            return $data;
        }

        $newData    = validate_collect($data);
        $processors = array_intersect_key($processors, array_flip($fields));
        foreach ($processors as $field => $callback) {
            if (null === $callback) {
                continue;
            }

            $dataProcessors = $this->processDataCondition($callback);
            if (!is_array($dataProcessors)) {
                $dataProcessors = [$dataProcessors];
            }

            if (str_contains($field, '*')) {
                $flatData = ValidationData::initializeAndGatherData($field, $data);
                $pattern  = str_replace('\*', '[^\.]*', preg_quote($field));
                foreach ($flatData as $key => $value) {
                    if (Str::startsWith($key, $field) || preg_match('/^' . $pattern . '\z/', $key)) {
                        foreach ($dataProcessors as $dataProcessor) {
                            $this->dataProcessing($key, $dataProcessor, $newData);
                        }
                    }
                }
            } else {
                foreach ($dataProcessors as $dataProcessor) {
                    $this->dataProcessing($field, $dataProcessor, $newData);
                }
            }
        }

        return $newData->toArray();
    }

    /**
     * @return DataProcessor[]|DataProcessor
     */
    private function processDataCondition(mixed $callback): DataProcessor|array
    {
        $execCond       = ProcessorExecCond::ALWAYS;
        $params         = [];
        $dataProcessors = [];
        $dataProcessor  = new DataProcessor();
        if (is_array($callback) && !empty($callback)) {
            $valueIsArray = false;

            if (in_array(ProcessorOptions::MULTIPLE, $callback)) {
                foreach ($callback as $item) {
                    if ($item instanceof ProcessorSupport) {
                        continue;
                    }
                    $dataProcessor    = $this->processDataCondition($item);
                    $dataProcessors[] = $dataProcessor;
                }
                return $dataProcessors;
            }

            $callback = array_filter($callback, function ($item) use (&$params, &$valueIsArray, &$execCond) {
                if ($item instanceof ProcessorExecCond) {
                    $execCond = $item;
                } elseif ($item instanceof ProcessorParams) {
                    $params[] = $item;
                    return false;
                } elseif ($item instanceof ProcessorSupport) {
                    if (ProcessorOptions::VALUE_IS_ARRAY === $item) {
                        $valueIsArray = true;
                    }
                    return false;
                }

                return true;
            });

            if (!$valueIsArray) {
                if (ProcessorExecCond::ALWAYS !== $execCond || !empty($params)) {
                    $callback = $callback[0];
                }
            }
        }

        $dataProcessor->execCond = $execCond;
        $dataProcessor->params   = $params;
        $dataProcessor->callback = $callback;
        return $dataProcessor;
    }

    /**
     * Applying preprocessor to the data.
     *
     * @param string        $field         Name of the data field to be processed
     * @param DataProcessor $dataProcessor Data processor
     * @param Collection    $data          Data to be processed
     */
    private function dataProcessing(
        string $field,
        DataProcessor $dataProcessor,
        Collection $data
    ): void {
        $isEmpty = function ($value) {
            return null === $value || [] === $value || '' === $value;
        };

        $value         = $data->get($field);
        $dataAttribute = new DataAttribute();

        if (ProcessorExecCond::WHEN_EMPTY === $dataProcessor->execCond && !$isEmpty($value)) {
            $data->set($field, $value);
            return;
        } elseif (ProcessorExecCond::WHEN_NOT_EMPTY === $dataProcessor->execCond && $isEmpty($value)) {
            $data->set($field, $value);
            return;
        }

        $params   = $dataProcessor->params;
        $callback = $dataProcessor->callback;

        if (empty($params)) {
            $callParams = [$value, $field, $this->checkData, $dataAttribute];
        } else {
            $callParams = array_map(function ($param) use ($value, $field, $dataAttribute) {
                return match ($param) {
                    ProcessorParams::Value         => $value,
                    ProcessorParams::Attribute     => $field,
                    ProcessorParams::OriginalData  => $this->checkData,
                    ProcessorParams::DataAttribute => $dataAttribute
                };
            }, $params);
        }

        if (is_callable($callback)) {
            $value = call_user_func($callback, ...$callParams);
        } elseif (
            (is_string($callback) || is_object($callback))
            && class_exists($callback)
            && is_subclass_of($callback, ProcessorInterface::class)
        ) {
            /** @var ProcessorInterface $callback */
            $callback = new $callback($dataAttribute);
            $value    = $callback->handle($value, $field, $this->checkData);
        } elseif (is_string($callback) && method_exists($this, lcfirst($callback) . 'Processor')) {
            $value = call_user_func([$this,  lcfirst($callback) . 'Processor'], ...$callParams);
        } else {
            $value = $callback;
        }

        if (true === $dataAttribute->deleteField) {
            $data->forget($field);
        } else {
            $data->set($field, $value);
        }
    }

    /**
     * 初始化验证器.
     */
    private function init(): void
    {
        $this->validatedData  = [];
        $this->validateFields = [];
        $this->initScene();
        $this->getSelfRules();
    }

    /**
     * 初始化验证场景.
     */
    private function initScene(): void
    {
        $this->afters         = [];
        $this->befores        = [];
        $this->events         = [];
        $this->preprocessors  = [];
        $this->postprocessors = [];
        $this->eventPriority  = true;
    }

    /**
     * 为验证器设置一个消息提供程序.
     *
     * @return $this
     *
     * @throws ValidateException
     */
    public function setMessageProvider(callable|MessageProviderInterface|string $messageProvider): static
    {
        if (is_string($messageProvider) && is_subclass_of($messageProvider, MessageProviderInterface::class)) {
            $this->messageProvider = new $messageProvider();
        } elseif (is_object($messageProvider) && is_subclass_of($messageProvider, MessageProviderInterface::class)) {
            $this->messageProvider = $messageProvider;
        } elseif (is_callable($messageProvider)) {
            $messageProvider = call_user_func($messageProvider);
            $this->setMessageProvider($messageProvider);
            return $this;
        } else {
            throw new ValidateRuntimeException('The provided message processor needs to implement the MessageProviderInterface interface');
        }

        return $this;
    }

    /**
     * 获取消息提供程序.
     */
    public function getMessageProvider(): MessageProviderInterface
    {
        if (empty($this->messageProvider)) {
            $this->messageProvider = new MessageProvider();
        }

        $messageProvider = $this->messageProvider;
        $messageProvider->setMessages($this->message);
        $messageProvider->setCustomAttributes($this->customAttributes);
        $messageProvider->setData($this->checkData);
        return $messageProvider;
    }

    /**
     * Add Event.
     *
     * @param string       $type     'event','before','after
     * @param string|array $callback
     */
    private function addEvent(string $type, $callback): void
    {
        $type .= 's';
        $callbacks = $this->$type;

        if (is_string($callback)) {
            $callbacks[] = [$callback, []];
        } else {
            foreach ($callback as $classOrMethod => $param) {
                if (is_int($classOrMethod)) {
                    $callbacks[] = [$param, []];
                } elseif (is_string($classOrMethod)) {
                    if (is_array($param)) {
                        $callbacks[] = [$classOrMethod, $param];
                    } else {
                        $callbacks[] = [$classOrMethod, [$param]];
                    }
                }
            }
        }

        $this->$type = $callbacks;
    }

    /**
     * 扩展规则.
     *
     * @see https://v.neww7.com/5/Rule.html#extend-%E6%89%A9%E5%B1%95%E6%96%B9%E6%B3%95
     *
     * @return $this
     */
    public function extend(string $rule, callable|\Closure|string $extension, string $message = null): static
    {
        $rule                    = Str::snake($rule);
        $this->extensions[$rule] = $extension;
        if (!is_null($message) && !isset($this->ruleMessage[$rule])) {
            $this->ruleMessage[$rule] = $message;
        }
        return $this;
    }

    /**
     * 扩展隐式规则.
     *
     * @see https://v.neww7.com/5/Rule.html#extendimplicit-%E9%9A%90%E5%BC%8F%E6%89%A9%E5%B1%95
     *
     * @return $this
     */
    public function extendImplicit(string $rule, callable|\Closure|string $extension, string $message = null): static
    {
        $rule = Str::snake($rule);
        if (!is_null($message) && !isset($this->ruleMessage[$rule])) {
            $this->ruleMessage[$rule] = $message;
        }
        $this->implicitExtensions[$rule] = $extension;
        return $this;
    }

    /**
     * 扩展依赖规则.
     *
     * @see https://v.neww7.com/5/Rule.html#extenddependent-%E4%BE%9D%E8%B5%96%E6%80%A7%E9%AA%8C%E8%AF%81%E5%99%A8
     *
     * @return $this
     */
    public function extendDependent(string $rule, callable|\Closure|string $extension, string $message = null): static
    {
        $rule = Str::snake($rule);
        if (!is_null($message) && !isset($this->ruleMessage[$rule])) {
            $this->ruleMessage[$rule] = $message;
        }
        $this->dependentExtensions[$rule] = $extension;
        return $this;
    }

    /**
     * 扩展错误信息替换器.
     *
     * @see https://v.neww7.com/5/Rule.html#replacer-%E9%94%99%E8%AF%AF%E4%BF%A1%E6%81%AF%E6%9B%BF%E6%8D%A2%E5%99%A8
     *
     * @return $this
     */
    public function extendReplacer(string $rule, callable|\Closure|string $replacer): static
    {
        $rule                   = Str::snake($rule);
        $this->replacers[$rule] = $replacer;
        return $this;
    }

    private function initRuleMessage(): void
    {
        foreach ($this->ruleMessage as $key => $message) {
            $snakeRule = Str::snake($key);
            if (isset($this->ruleMessage[$snakeRule])) {
                continue;
            }

            $studlyRule = Str::studly($key);
            if (isset($this->ruleMessage[$studlyRule])) {
                $this->ruleMessage[$snakeRule] = $this->ruleMessage[$studlyRule];
                unset($this->ruleMessage[$studlyRule]);
            }

            $camelRule = Str::camel($key);
            if (isset($this->ruleMessage[$camelRule])) {
                $this->ruleMessage[$snakeRule] = $this->ruleMessage[$camelRule];
                unset($this->ruleMessage[$camelRule]);
            }
        }
    }
}
