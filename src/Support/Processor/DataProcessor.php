<?php

namespace W7\Validate\Support\Processor;

class DataProcessor
{
    public function __construct(
        public mixed $callback = null,
        public ProcessorExecCond $execCond = ProcessorExecCond::ALWAYS,
        public array $params = []
    ) {

    }
}
