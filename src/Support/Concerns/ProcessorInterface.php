<?php

namespace W7\Validate\Support\Concerns;

interface ProcessorInterface
{
    public function handle($value, string $attribute, array $originalData);
}
