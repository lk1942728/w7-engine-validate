<?php

namespace W7\Validate\Support\Rule;

use Itwmw\Validation\Support\Arr;

/**
 * Custom Rules.
 *
 * @see https://v.neww7.com/en/5/Rule.html#using-rule-objects
 */
abstract class BaseRule implements RuleInterface
{
    /**
     * Error messages, support for format strings.
     *
     * @var string
     */
    protected $message = '';

    /**
     * Parameters for format error messages.
     *
     * @var array
     */
    protected $messageParam = [];

    public function setMessage(string $message): BaseRule
    {
        $this->message = $message;
        return $this;
    }

    public function getMessage(): string
    {
        $message = $this->message;
        if (preg_match_all('/%{(.*?)}/', $message, $matches) > 0) {
            $data = get_object_vars($this);
            foreach ($matches[0] as $index => $pregString) {
                $value   = Arr::get($data, $matches[1][$index], '');
                $value   = is_scalar($value) ? $value : '';
                $message = str_replace($pregString, $value, $message);
            }
        }
        return vsprintf($message, $this->messageParam);
    }

    public function message(): string
    {
        return $this->getMessage();
    }

    public static function make(...$params): BaseRule
    {
        return new static(...$params);
    }

    public function check($data): bool
    {
        return $this->passes('', $data);
    }
}
